﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Content;
using UnityEditor.Build.Pipeline;
using UnityEditor.Build.Pipeline.Interfaces;

public static class PerBundleCompressionExample
{
    // New parameters class inheriting from BundleBuildParameters
    class CustomBuildParameters : BundleBuildParameters
    {
        public static UnityEngine.CompressionType MyCompressionType = UnityEngine.CompressionType.None;

        public Dictionary<string, UnityEngine.BuildCompression> PerBundleCompression { get; set; }

        public CustomBuildParameters(BuildTarget target, BuildTargetGroup group, string outputFolder) : base(target, group, outputFolder)
        {
            PerBundleCompression = new Dictionary<string, UnityEngine.BuildCompression>();
        }

        // Override the GetCompressionForIdentifier method with new logic
        public override UnityEngine.BuildCompression GetCompressionForIdentifier(string identifier)
        {
            UnityEngine.BuildCompression value;
            if (PerBundleCompression.TryGetValue(identifier, out value))
                return value;
            return BundleCompression;
        }
    }

    public static bool BuildAssetBundles(string outputPath, bool useChunkBasedCompression, BuildTarget buildTarget, BuildTargetGroup buildGroup, UnityEngine.CompressionType compressionType)
    {
        var buildContent = new BundleBuildContent(ContentBuildInterface.GenerateAssetBundleBuilds());
        // Construct the new parameters class
        var buildParams = new CustomBuildParameters(buildTarget, buildGroup, outputPath);
        // Populate the bundle specific compression data
        buildParams.PerBundleCompression.Add("Bundle1", UnityEngine.BuildCompression.Uncompressed);
        buildParams.PerBundleCompression.Add("Bundle2", UnityEngine.BuildCompression.LZMA);

        if (compressionType == UnityEngine.CompressionType.None)
            buildParams.BundleCompression = UnityEngine.BuildCompression.Uncompressed;
        else if (compressionType == UnityEngine.CompressionType.Lzma)
            buildParams.BundleCompression = UnityEngine.BuildCompression.LZMA;
        else if (compressionType == UnityEngine.CompressionType.Lz4 || compressionType == UnityEngine.CompressionType.Lz4HC)
            buildParams.BundleCompression = UnityEngine.BuildCompression.LZ4;

        IBundleBuildResults results;
        ReturnCode exitCode = ContentPipeline.BuildAssetBundles(buildParams, buildContent, out results);
        return exitCode == ReturnCode.Success;
    }
}

#endif