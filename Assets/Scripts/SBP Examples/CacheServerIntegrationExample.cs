﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Build.Content;
using UnityEditor.Build.Pipeline;
using UnityEditor.Build.Pipeline.Interfaces;

public static class CacheServerIntegrationExample
{
    public static bool BuildAssetBundles(string outputPath, bool useChunkBasedCompression, BuildTarget buildTarget, BuildTargetGroup buildGroup, string cacheServerHost, int cacheServerPort)
    {
        var buildContent = new BundleBuildContent(ContentBuildInterface.GenerateAssetBundleBuilds());
        var buildParams = new BundleBuildParameters(buildTarget, buildGroup, outputPath);
        // Set build parameters for connecting to the Cache Server
        buildParams.UseCache = true;
        buildParams.CacheServerHost = cacheServerHost; 
        buildParams.CacheServerPort = cacheServerPort;

        if (useChunkBasedCompression)
            buildParams.BundleCompression = UnityEngine.BuildCompression.LZ4;

        IBundleBuildResults results;
        ReturnCode exitCode = ContentPipeline.BuildAssetBundles(buildParams, buildContent, out results);
        return exitCode == ReturnCode.Success;
    }
}

#endif