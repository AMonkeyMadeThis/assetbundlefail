﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

public class BuildMenu : MonoBehaviour
{
    #region Constants

    const string ASSET_BUNDLE_PATH = "Assets/StreamingAssets/AssetBundles";
    const bool FORCE_REBUILD = false;
    const bool USE_CHUNK_BASED_COMPRESSION = false;
    const BuildTarget BUILD_TARGET = BuildTarget.StandaloneWindows;
    const BuildTargetGroup BUILD_TARGET_GROUP = BuildTargetGroup.Standalone;
    const CompressionType COMPRESSION_TYPE = CompressionType.None;

    #endregion

    #region Basic Example

    [MenuItem("Asset Bundles/Basic/Current (non-SBP)")]
    public static void BuildBasicCurrent()
    {
        BasicExampleCurrent.BuildAssetBundles(ASSET_BUNDLE_PATH, FORCE_REBUILD, USE_CHUNK_BASED_COMPRESSION, BUILD_TARGET);
    }

    [MenuItem("Asset Bundles/Basic/Example")]
    public static void BuildBasicExample()
    {
        BasicExample.BuildAssetBundles(ASSET_BUNDLE_PATH, FORCE_REBUILD, USE_CHUNK_BASED_COMPRESSION, BUILD_TARGET);
    }

    #endregion

    #region Cache Server

    [MenuItem("Asset Bundles/Cache Server/Example")]
    public static void BuildCacheServer()
    {
        // Use your own host because "buildcache.unitygames.com" will refuse the connection
        CacheServerIntegrationExample.BuildAssetBundles(ASSET_BUNDLE_PATH, USE_CHUNK_BASED_COMPRESSION, BUILD_TARGET, BUILD_TARGET_GROUP, "buildcache.unitygames.com", 8126);
    }

    #endregion

    #region Per-bundle compression

    [MenuItem("Asset Bundles/Per-Bundle compression/Example")]
    public static void BuildPerBundleCompression()
    {
        PerBundleCompressionExample.BuildAssetBundles(ASSET_BUNDLE_PATH, USE_CHUNK_BASED_COMPRESSION, BUILD_TARGET, BUILD_TARGET_GROUP, COMPRESSION_TYPE);
    }

    #endregion

    #region Load by file name

    [MenuItem("Asset Bundles/Per-Bundle compression/Example")]
    public static void BuildLoadByFileName()
    {
        PerBundleCompressionExample.BuildAssetBundles(ASSET_BUNDLE_PATH, USE_CHUNK_BASED_COMPRESSION, BUILD_TARGET, BUILD_TARGET_GROUP, COMPRESSION_TYPE);
    }

    #endregion
}

#endif